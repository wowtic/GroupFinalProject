﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Models.Models.eNums
{
    public enum Phase
    {
        Open = 0,
        Voting = 1,
        Closed = 2
    }
}
