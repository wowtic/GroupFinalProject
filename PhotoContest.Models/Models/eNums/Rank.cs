﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Models.Models.eNums
{
    public enum Rank
    {
        Junkie = 0,
        Enthusiast = 1,
        Master = 2,
        PhotoDictator = 3
    }
}
