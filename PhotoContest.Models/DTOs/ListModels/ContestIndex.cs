﻿using System.Collections.Generic;

namespace PhotoContest.Models.DTOs.ListModels
{
    public class ContestIndex
    {
        public IEnumerable<ContestDTO> ContestList { get; set; }
    }
}
